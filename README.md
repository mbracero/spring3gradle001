## Apuntes

**Spring3 (boot, mvc) + gradle + CORS**

## No poner solo un constructor con parametros en las clases del modelo, aniadir el constructor por defecto. Si no hacemos esto nos da un error.


**CURL**

curl -v -H "Accept: application/json" -H "Content-type: application/json" -X GET http://localhost:9000/api/items



curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"id": 2, "name": "ABCD"}' http://localhost:9000/api/items.json





**POSTMAN**

![POSTMAN.png](https://bitbucket.org/repo/L6qLAr/images/2674562764-POSTMAN.png)