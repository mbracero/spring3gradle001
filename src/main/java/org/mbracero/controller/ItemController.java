package org.mbracero.controller;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletResponse;

import org.mbracero.model.Item;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ItemController extends BaseController {
	private Collection<Item> items = new CopyOnWriteArrayList<Item>();
	private static final AtomicLong currentId = new AtomicLong(0L);
	
	@RequestMapping(value="/items", method=RequestMethod.GET)
	public @ResponseBody Collection<Item> getItems() {
		return items;
	}
	
	@RequestMapping(value="/items.json", method=RequestMethod.POST)
	public @ResponseBody Item addItem(
			@RequestBody Item item,
			HttpServletResponse response) {
		if (item.getId() == null || item.getId() <= 0) {
			item.setId(currentId.incrementAndGet());
		}
		
		items.add(item);
		
		response.addHeader("Location", "/items/" + item.getId());
		return item;
	}
	
	@RequestMapping(value="/items.form", method=RequestMethod.POST)
	public @ResponseBody Item addItem2(
			@RequestParam(value="id") Long id,
			@RequestParam(value="name") String name,
			HttpServletResponse response) {
		if (id == null || id <= 0) {
			id = currentId.incrementAndGet();
		}
		Item item = new Item();
		item.setId(id);
		item.setName(name);
		
		items.add(item);
		
		response.addHeader("Location", "/api/items/" + item.getId());
		return item;
	}
	
	@RequestMapping(value="/items/{id}", method=RequestMethod.GET)
	public @ResponseBody Item getItem(@PathVariable(value="id") long id) {
		Item ret = null;
		for (Item item : items) {
			if (item.getId().equals(id)) {
				ret = item;
			}
		}
		return ret;
	}
}
